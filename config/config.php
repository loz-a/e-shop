<?php

use Zend\Expressive\ConfigManager\ConfigManager;
use Zend\Expressive\ConfigManager\PhpFileProvider;
use Zend\I18n\ConfigProvider as I18nConfigProvider;
use Zend\Form\ConfigProvider as FormConfigProvider;

$configManager = new ConfigManager([
    new PhpFileProvider('config/autoload/{{,*.}global,{,*.}local}.php'),
    I18nConfigProvider::class,
    FormConfigProvider::class,
    App\AppConfig::class,
    Index\IndexConfig::class,
    Admin\AdminConfig::class,
    Auth\AuthConfig::class
]);

return new ArrayObject($configManager->getMergedConfig());


