<?php

return [
    'dependencies' => [
        'invokables' => [
            Index\Action\PingAction::class => Index\Action\PingAction::class,
        ],
        'factories' => [
            Index\Action\HomePageAction::class => Index\Action\HomePageFactory::class,
        ],
    ],

    'routes' => [
        [
            'name' => 'home',
            'path' => '/',
            'middleware' => Index\Action\HomePageAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'api.ping',
            'path' => '/api/ping',
            'middleware' => Index\Action\PingAction::class,
            'allowed_methods' => ['GET'],
        ],
    ],


    'templates' => [
//        'layout' => 'layout::default',
        'map' => [
            'layout::default' => __DIR__ . '/../templates/layout/default.phtml',
            'error::error'    => __DIR__ . '/../templates/error/error.phtml',
            'error::404'      => __DIR__ . '/../templates/error/404.phtml',
        ],
        'paths' => [
            'index'    => [__DIR__ . '/../templates/index'],
            'layout' => [__DIR__ . '/../templates/layout'],
            'error'  => [__DIR__ . '/../templates/error'],
        ],
    ]
];
