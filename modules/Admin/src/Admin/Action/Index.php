<?php
namespace Admin\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Index
{
    /**
     * @var callable
     */
    private $responder;

    public function __construct(callable $responder)
    {
        $this->responder = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ){
        $responder = $this->responder;

        return $responder();
    } // __invoke()
}