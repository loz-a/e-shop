<?php
namespace Admin\Responder;

use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class Index
{
    /**
     * @var TemplateRendererInterface
     */
    private $template;

    public function __construct(TemplateRendererInterface $template)
    {
        $this->template = $template;
    } // __construct()

    public function __invoke()
    {
        $data = ['layout' => 'layout::admin'];

        return new HtmlResponse(
            $this->template->render('admin::index', $data)
        );
    } // __invoke()
}