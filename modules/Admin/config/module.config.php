<?php

return [
    'dependencies' => [
        'invokables' => [

        ],
        'factories' => [
            Admin\Action\Index::class => Admin\Action\IndexFactory::class
        ]
    ],


    'routes' => [
        [
            'name' => 'admin',
            'path' => '/admin',
            'middleware' => Admin\Action\Index::class,
            'allowed_methods' => ['GET'],
            'options' => [
                'defaults' => [
                    'login_required' => true,
                ]
            ]
        ]
    ],


    'templates' => [
        'map' => [
            'layout::admin' => __DIR__ . '/../templates/layout/admin.phtml',
            'admin::index' => __DIR__ . '/../templates/admin/index.phtml'
        ],
        'paths' => [
            'admin'    => [__DIR__ . '/../templates/admin'],
            'layout' => [__DIR__ . '/../templates/layout'],
        ],
    ]
];