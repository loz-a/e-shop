<?php
namespace Auth\Model\Repository;

use Auth\Model\Entity\BlockedUser;
use Auth\Model\Storage\LockoutInterface as LockoutStorageInterface;

class Lockout implements LockoutInterface
{
    /**
     * @var LockoutStorageInterface
     */
    protected $lockoutStorage;

    public function __construct(LockoutStorageInterface $lockoutStorage)
    {
        $this->lockoutStorage = $lockoutStorage;
    } // __construct()


    public function findByLogin(string $login)
    {
        return $this->lockoutStorage->findByLogin($login);
    }


    public function insert(BlockedUser $blockedUser) : bool
    {
        return $this->lockoutStorage->insert($blockedUser);
    } // insert()


    public function update(BlockedUser $blockedUser) : bool
    {
        return $this->lockoutStorage->update($blockedUser);
    }


    public function delete(BlockedUser $blockedUser) : bool
    {
        return $this->lockoutStorage->delete($blockedUser);
    } // delete()

}