<?php
namespace Auth\Model\Repository;

use Auth\Model\Entity\User;
use Auth\Model\Storage\UsersInterface as UsersStorageInterface;

class Users implements UsersInterface
{
    /**
     * @var UsersStorageInterface
     */
    protected $usersStorage;

    public function __construct(UsersStorageInterface $usersStorage)
    {
        $this->usersStorage = $usersStorage;
    } // __construct()


    /**
     * @param $id
     * @return User|null
     */
    public function findUserById(int $id)
    {
        return $this->usersStorage->findUserById($id);
    }


    /**
     * @param $login
     * @return User|null
     */
    public function findUserByLogin(string $login)
    {
        return $this->usersStorage->findUserByLogin($login);
    }

}