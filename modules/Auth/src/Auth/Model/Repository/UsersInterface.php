<?php
namespace Auth\Model\Repository;

use Auth\Model\Entity\User;

interface UsersInterface
{
    /**
     * @param $id
     * @return User|null
     */
    public function findUserById(int $id);

    /**
     * @param $login
     * @return User|null
     */
    public function findUserByLogin(string $login);

}