<?php
namespace Auth\Model\Storage;

interface UsersInterface
{
    public function findUserById(int $id);

    public function findUserByLogin(string $login);
}