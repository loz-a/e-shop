<?php
namespace Auth\Model\Storage\Db;

use Auth\Model\Entity\BlockedUser;
use Auth\Model\Storage\LockoutInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Sql;

class Lockout extends AbstractDbStorage
    implements LockoutInterface
{
    /**
     * @var string
     */
    protected $usersTable;

    public function __construct(string $table, string $usersTable, AdapterInterface $dbAdapter)
    {
        parent::__construct($table, $dbAdapter);
        $this->usersTable = $usersTable;
    }

    public function findByLogin(string $login)
    {
        $sql = new Sql($this->dbAdapter);
        $select = $sql
            ->select()
            ->columns(['id'])
            ->from(['u' => $this->usersTable])
            ->join(['l' => $this->table], 'u.id = l.id', ['lock_time', 'lock_tries'], 'left')
            ->where(['u.login' => $login])
            ->limit(1);

        $result = $sql->prepareStatementForSqlObject($select)->execute();

        if ($result->count()) {
            $userData = $result->current();
            $user     = new BlockedUser();

            return $user
                ->setId($userData['id'])
                ->setLockTime(intval($userData['lock_time']))
                ->setLockTries(intval($userData['lock_tries']));
        }
        return null;
    } // findByLogin()


    public function insert(BlockedUser $blockedUser) : bool
    {
        $sql = new Sql($this->dbAdapter);

        $insert = $sql->insert($this->table);
        $insert->values([
            'id'         => $blockedUser->getId(),
            'lock_tries' => $blockedUser->getLockTries(),
            'lock_time' => $blockedUser->getLockTime()
        ]);

        return (bool) $sql->prepareStatementForSqlObject($insert)->execute();
    } // insert()


    public function update(BlockedUser $blockedUser) : bool
    {
        $sql = new Sql($this->dbAdapter);

        $data = [
            'lock_tries' => $blockedUser->getLockTries(),
            'lock_time' => $blockedUser->getLockTime()
        ];

        $update = $sql->update($this->table);
        $update->set($data);
        $update->where(['id' => $blockedUser->getId()]);

        return (bool) $sql->prepareStatementForSqlObject($update)->execute();
    } // update()


    public function delete(BlockedUser $blockedUser) : bool
    {
        $sql = new Sql($this->dbAdapter);

        $delete = $sql->delete($this->table);
        $delete->where(['id' => $blockedUser->getId()]);

        return (bool) $sql->prepareStatementForSqlObject($delete)->execute();
    } // delete()

}