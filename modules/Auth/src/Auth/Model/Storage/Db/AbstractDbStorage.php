<?php
namespace Auth\Model\Storage\Db;

use Zend\Db\Adapter\AdapterInterface;

abstract class AbstractDbStorage
{
    /**
     * @var AdapterInterface
     */
    protected $dbAdapter;

    /**
     * @var string
     */
    protected $table;

    public function __construct(string $table, AdapterInterface $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
        $this->table = $table;

    } // __construct()
}