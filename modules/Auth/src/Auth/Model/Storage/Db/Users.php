<?php
namespace Auth\Model\Storage\Db;

use Auth\Model\Entity\User;
use Auth\Model\Storage\UsersInterface;
use Zend\Db\Sql\Sql;

class Users extends AbstractDbStorage
    implements UsersInterface
{
    /**
     * @param $id
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function findUserById(int $id)
    {
        $sql       = new Sql($this->dbAdapter);
        $select    = $sql->select($this->table)->where(['id' => $id]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute($select);

        return $result->count() ? $this->hydrateUser($result->current()) : null;
    }

    public function findUserByLogin(string $login)
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql
            ->select($this->table)
            ->where(['login' => $login])
            ->limit(1);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute($select);

        return $result->count() ? $this->hydrateUser($result->current()) : null;
    }


    protected function hydrateUser($userData)
    {
        $user = new User();
        $user->setId($userData['id']);
        $user->setLogin($userData['login']);
        $user->setPassword($userData['password']);
        $user->setSalt($userData['salt']);
        return $user;
    }

}