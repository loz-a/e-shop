<?php
namespace Auth\Model\Storage;

use Auth\Model\Entity\BlockedUser;

interface LockoutInterface
{
    public function findByLogin(string $login);

    public function insert(BlockedUser $blockedUser) : bool;

    public function update(BlockedUser $blockedUser) : bool;

    public function delete(BlockedUser $blockedUser) : bool;
}