<?php
namespace Auth\Model\Entity;


class BlockedUser
{
    protected $id;

    protected $lockTime;

    protected $lockTries;


    public function getId() : int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }


    public function getLockTime() : int
    {
        return $this->lockTime;
    }

    public function setLockTime(int $lockTime)
    {
        $this->lockTime = $lockTime;
        return $this;
    }


    public function getLockTries() : int
    {
        return $this->lockTries;
    }

    public function setLockTries(int $lockTries)
    {
        $this->lockTries = $lockTries;
        return $this;
    }

}