<?php
namespace Auth\Model\Entity;

class User
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $salt;

    public function setId(int $id)
    {
        $this -> id = $id;
        return $this;
    }

    public function getId() : int
    {
        return $this -> id;
    }


    public function setLogin(string $login)
    {
        $this -> login = $login;
        return $this;
    }

    public function getLogin() : string
    {
        return $this -> login;
    }


    public function setPassword(string $password)
    {
        $this -> password = $password;
        return $this;
    }

    public function getPassword() : string
    {
        return $this -> password;
    }


    public function setSalt(string $salt)
    {
        $this -> salt = $salt;
        return $this;
    } // setSalt()


    public function getSalt() : string
    {
        return $this -> salt;
    } // getSalt()

} // User