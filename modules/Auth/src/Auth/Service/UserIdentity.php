<?php
namespace Auth\Service;

use Auth\Model\Repository\UsersInterface as UsersRepositoryInterface;
use Zend\Authentication\AuthenticationServiceInterface;
use stdClass;

class UserIdentity
{
    /**
     * @var AuthenticationServiceInterface
     */
    protected $authService;

    /**
     * @var UsersRepositoryInterface
     */
    protected $usersRepository;

    /**
     * @var \stdClass
     */
    protected $user;


    public function __construct(
        AuthenticationServiceInterface $authService,
        UsersRepositoryInterface $usersRepository
    ){
        $this->authService = $authService;
        $this->usersRepository = $usersRepository;
    } // __construct()

    /**
     * @return \stdClass
     */
    public function getIdentity() : stdClass
    {
        if (!$this->authService->hasIdentity()) {
            return $this->user = null;
        }

        $identity = $this->authService->getIdentity();

        if (!$this->user
            || $this->user->id != $identity
        ) {
            $user = $this->usersRepository->findUserById($identity);

            if (!$user) {
                return $this->user = null;
            }

            $this->user = new \stdClass();
            $this->user->id    = $user->getId();
            $this->user->login = $user->getLogin();
            $this->user->salt  = $user->getSalt();
        }
        return $this->user;
    } // getIdentity()


    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->authService->hasIdentity()
            ? $this->getIdentity()->{$name} : null;
    } // __get()


    /**
     * @return bool
     */
    public function hasIdentity() : bool
    {
        return $this -> authService -> hasIdentity();
    } // hasIdentity()

} // UserIdentity
