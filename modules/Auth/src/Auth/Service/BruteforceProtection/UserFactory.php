<?php
namespace Auth\Service\BruteforceProtection;

use Auth\Options\ModuleOptions;
use Auth\Model\Repository\LockoutInterface;
use Interop\Container\ContainerInterface;

class UserFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $options    = $container->get(ModuleOptions::class);
        $repository = $container->get(LockoutInterface::class);

        return new User($options, $repository);
    } // __invoke()
}