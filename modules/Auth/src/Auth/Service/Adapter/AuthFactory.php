<?php
namespace Auth\Service\Adapter;

use Interop\Container\ContainerInterface;
use Auth\Options\ModuleOptions;
use Auth\Model\Repository\UsersInterface;

class AuthFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $usersRepository = $container->get(UsersInterface::class);
        $authOptions     = $container->get(ModuleOptions::class);

        $passwdStrategy  = new Password\Bcrypt();
        $passwdStrategy->setCost($authOptions->getPasswordCost());

        return new Auth($usersRepository, $passwdStrategy);
    } // __invoke()
}