<?php
namespace Auth\Service\Adapter;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Adapter\ValidatableAdapterInterface;

interface AuthInterface extends
    AdapterInterface, ValidatableAdapterInterface
{

}