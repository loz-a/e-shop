<?php
namespace Auth\Service\Adapter\Password;

use Zend\Crypt\Password\Bcrypt as ZendBcrypt;
use Zend\Crypt\Password\PasswordInterface as ZendPasswordInterface;

class Bcrypt implements StrategyInterface
{
    protected $cost;

    protected $salt;

    public function password() : ZendPasswordInterface
    {
        $bcrypt = new ZendBcrypt();

        if ($this->cost) {
            $bcrypt->setCost($this->cost);
        }

        if ($this->salt) {
            $bcrypt->setSalt($this->salt);
        }
        
        return $bcrypt;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    } // setCost()


    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    } // setSalt()

}