<?php
namespace Auth\Service;

use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Auth\Service\Adapter\AuthInterface as AuthAdapterInterface;

class ZendAuthFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authAdapter = $container->get(AuthAdapterInterface::class);
        $authService = new AuthenticationService();
        $authService->setAdapter($authAdapter);
        return $authService;
    } // __invoke()
}