<?php
namespace Auth\Service;

use Auth\Model\Repository\UsersInterface as UsersRepositoryInterface;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationServiceInterface;

class UserIdentityFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService     = $container->get(AuthenticationServiceInterface::class);
        $usersRepository = $container->get(UsersRepositoryInterface::class);

        return new UserIdentity($authService, $usersRepository);
    } // __invoke()

} // UserIdentity
