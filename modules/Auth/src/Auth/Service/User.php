<?php
namespace Auth\Service;

use Auth\Options\AuthInterface as AuthOptions;
use Auth\Model\Repository\UsersInterface as UsersRepositoryInterface;
use Auth\Service\Adapter\Password\StrategyInterface as PasswordStrategyInterface;
use Zend\Crypt\Password\Bcrypt;
use Zend\Math\Rand;
use Auth\Model\Entity\User as UserEntity;

class User
{
    /**
     * @var AuthOptions
     */
    protected $authOptions;

    /**
     * @var UsersRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var UserIdentity
     */
    protected $userIdentity;

    /**
     * @var PasswordStrategyInterface
     */
    protected $passwordStrategy;


    public function __construct(
        AuthOptions $options,
        UsersRepositoryInterface $userRepository,
        UserIdentity $userIdentity,
        PasswordStrategyInterface $passwordStrategy
    ){
        $this->authOptions      = $options;
        $this->userRepository   = $userRepository;
        $this->userIdentity     = $userIdentity;
        $this->passwordStrategy = $passwordStrategy;
    } // __construct()


    public function changePassword($newPassword)
    {
        if (!$this->userIdentity->hasIdentity()) {
            return false;
        }

        $identity = $this->userIdentity->getIdentity();

        $saltSize = $this->authOptions->getSaltSize();
        $salt     = Rand::getString($saltSize);

        $this->passwordStrategy->setSalt($salt);
        $cryptPassword = $this->passwordStrategy->password()->create($newPassword);

        $user = new UserEntity();
        $user
            ->setId($identity->id)
            ->setPassword($cryptPassword)
            ->setSalt($salt);

        return (bool) $this->userRepository->update($user);
    } // changePassword()

}