<?php
namespace Auth\Service;

use Interop\Container\ContainerInterface;
use Auth\Options\ModuleOptions;
use Zend\Authentication\AuthenticationServiceInterface;

class AuthFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $zendAuthService = $container->get(AuthenticationServiceInterface::class);
        return new Auth($zendAuthService);
    } // __invoke()
}