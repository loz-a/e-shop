<?php
namespace Auth\Middleware;

use Auth\Service\UserIdentity;
use Auth\Tranlator\AuthTranslatorTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\I18n\Translator\TranslatorInterface;

class LoginRequired
{
    use AuthTranslatorTrait;

    /**
     * @var UserIdentity
     */
    private $userIdentity;

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(
        UserIdentity $userIdentity,
        RouterInterface $router,
        TranslatorInterface $translator
    ){
        $this->userIdentity = $userIdentity;
        $this->router       = $router;
        $this->translator   = $translator;
    } // __construct()

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ) {
        $isLoginRequired = $request->getAttribute('login_required', false);

        if ($isLoginRequired
            && !$this->userIdentity->hasIdentity()
        ) {
            $flash = $request->getAttribute('flash');
            $flash->addWarningMessage($this->translate('Access Denied'));

            return new RedirectResponse(
                $this->router->generateUri('home')
            );
        }
        return $next($request, $response);
    }
}