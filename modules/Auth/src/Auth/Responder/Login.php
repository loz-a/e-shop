<?php
namespace Auth\Responder;

use Auth\AuthConfig;
use Auth\Form\Login as LoginForm;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class Login
{
    /**
     * @var TemplateRendererInterface
     */
    private $template;

    public function __construct(
        TemplateRendererInterface $template
    ){
        $this->template = $template;
    } // __construct()

    public function __invoke(LoginForm $loginForm)
    {
        $data = [
            'layout' => 'layout::auth',
            'form'   => $loginForm,
            'translationTextDomain' => AuthConfig::MODULE_NAME
        ];

        return new HtmlResponse(
            $this->template->render('auth::login', $data)
        );
    } // __invoke()
}