<?php
namespace Auth\Action;

use Auth\Options\AuthInterface as AuthOptionsInterface;
use Auth\Form\Login as LoginForm;
use Auth\Service\BruteforceProtection\User as UserProtectionService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\I18n\Translator\TranslatorInterface;
use Auth\Tranlator\AuthTranslatorTrait;

class UserBruteforceProtection
{
    use AuthTranslatorTrait;

    /**
     * @var AuthOptionsInterface
     */
    protected $authOptions;
    /**
     * @var UserProtectionService
     */
    protected $userProtectionService;

    /**
     * @var LoginForm
     */
    private $loginForm;

    /**
     * @var callable
     */
    private $responder;

    public function __construct(
        AuthOptionsInterface $authOptions,
        UserProtectionService $service,
        LoginForm $loginForm,
        TranslatorInterface $translator,
        callable $responder
    ){
        $this->authOptions = $authOptions;
        $this->userProtectionService = $service;
        $this->loginForm   = $loginForm;
        $this->translator  = $translator;
        $this->responder   = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        if ($request->getMethod() === 'GET') {
            return $next($request, $response);
        }

        $params = $request->getParsedBody();
        $login  = isset($params['login']) ? $params['login'] : null;

        if (!$login) {
            return $this->returnLoginResponderWithErrorMessage(
                $request,
                $this->translate('Authorization error. Please check login or/and password')
            );
        }

        $isLocked = $this->userProtectionService->isLocked($params['login']);

        if ($isLocked) {
            return $this->returnLoginResponderWithErrorMessage(
                $request,
                sprintf(
                    $this->translate('Authorization error. Please, expect the next %s seconds to login'),
                    $this->authOptions->getWaitTime()
                )
            );
        }

        $result = $next($request, $response);

        if ($result instanceof ResponseInterface) {
            $statusCode = $result->getStatusCode();

            if ($login) {
                if ($statusCode !== 302) { // if not redirect to admin
                    $this->userProtectionService->increaseTriesCounter($login);
                }
            }
        }

        return $result;
    }


    protected function returnLoginResponderWithErrorMessage($request, $msg)
    {
        $flash     = $request->getAttribute('flash');
        $responder = $this->responder;

        $flash->addErrorMessage($msg);
        return $responder($this->loginForm);
    }
}