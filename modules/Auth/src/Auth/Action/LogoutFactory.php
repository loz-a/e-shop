<?php
namespace Auth\Action;

use Auth\Service\Auth as AuthService;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;

class LogoutFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $router      = $container->get(RouterInterface::class);

        return new Logout($authService, $router);
    } // __invoke()
}