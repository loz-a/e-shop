<?php
namespace Auth\Action;

use Auth\Form\Login as LoginForm;
use Auth\Responder\Login as LoginResponder;
use Auth\Service\Auth as AuthService;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\I18n\Translator\TranslatorInterface;

class LoginFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $loginForm   = $container->get(LoginForm::class);
        $router      = $container->get(RouterInterface::class);
        $translator  = $container->get(TranslatorInterface::class);
        $template    = $container->get(TemplateRendererInterface::class);
        $responder   = new LoginResponder($template);

        return new Login($authService, $loginForm, $router, $translator, $responder);
    } // __invoke()
}