<?php
namespace Auth\Action;

use Auth\Form\Login as LoginForm;
use Auth\Service\Auth as AuthService;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\I18n\Translator\TranslatorInterface;
use Auth\Tranlator\AuthTranslatorTrait;

class Login
{
    use AuthTranslatorTrait;

    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var LoginForm
     */
    private $loginForm;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var callable
     */
    private $responder;

    public function __construct(
        AuthService $authService,
        LoginForm $loginForm,
        RouterInterface $router,
        TranslatorInterface $translator,
        callable $responder
    ){
        $this->authService = $authService;
        $this->loginForm   = $loginForm;
        $this->router      = $router;
        $this->translator  = $translator;
        $this->responder   = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        if ($request->getMethod() === 'POST') {

            $flash  = $request->getAttribute('flash');
            $params = $request->getParsedBody();

            $this->loginForm->setData($params);

            if ($this->loginForm->isValid()) {
                $result = $this->authService->login($params['login'], $params['password']);

                if ($result->isValid()) {
                    $flash->addSuccessMessage($this->translate('Now you are logging'));

                    return new RedirectResponse(
                        $this->router->generateUri('admin')
                    );
                }
            }

            $flash->addErrorMessage(
                $this->translate('Authorization error. Please check login or/and password')
            );
            sleep(1); // small bruteforce shield
        }

        $responder = $this->responder;
        return $responder($this->loginForm);
    } // __invoke()
}