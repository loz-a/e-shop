<?php
namespace Auth\Form;

use Auth\AuthConfig;
use Zend\Form\Form;
use Zend\Form\Element\Password;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Submit;
use Zend\I18n\Translator\Translator;

class Login extends Form
{
    /**
     * @var Translator
     */
    protected $translator;

    public function __construct(Translator $translator)
    {
        parent::__construct();

        $this->translator = $translator;
    } // setTranslator()


    public function init()
    {
        $this
            -> setName('Sign in')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this->add([
            'name' => 'login',
            'options' => [
                'label' => 'Login'
            ],
            'attributes' => [
                'placeholder' => $this->translate('Login'),
                'class'       => 'form-control',
                'autofocus'   => true
            ]
        ]);

        $this->add([
            'name' => 'password',
            'type' => Password::class,
            'options' => [
                'type' => 'password'
            ],
            'attributes' => [
                'placeholder' => $this->translate('Password'),
                'class'       => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'csrf',
            'type' => Csrf::class,
            'options' => [
                'csrf_options' => [
                    'timeout' => 6000
                ]
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Submit::class,
            'attributes' => [
                'type'  => 'submit',
                'class' => 'btn btn-lg btn-primary btn-block',
                'value' => $this->translate('Sign in')
            ]
        ]);

    } // init()


    protected function translate($message)
    {
        return $this->translator->translate($message, AuthConfig::MODULE_NAME);
    }
}