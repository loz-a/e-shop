<?php
namespace Auth\Tranlator;

use Auth\AuthConfig;
use Zend\I18n\Translator\TranslatorInterface;

trait AuthTranslatorTrait
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    protected function translate($message)
    {
        $translatorTextDomain = AuthConfig::MODULE_NAME;
        return $this->translator->translate($message, $translatorTextDomain);
    }
}