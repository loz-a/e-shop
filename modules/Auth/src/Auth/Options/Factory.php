<?php
namespace Auth\Options;

use Interop\Container\ContainerInterface;

class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $options = isset($config['auth_options']) ? $config['auth_options'] : [];
        return new ModuleOptions($options);
    }
}