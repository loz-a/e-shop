<?php
namespace Auth\Options;

interface AuthInterface
{
    public function setLockTries($lockTries);
    public function getLockTries();

    public function setWaitTime($lockTime);
    public function getWaitTime();

    public function setPasswordCost($cost);
    public function getPasswordCost();

    public function setSaltSize($size);
    public function getSaltSize();
}