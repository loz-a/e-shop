<?php
use Auth\Core\Service;

use Core\AppTestCase;
use Core\AbstractDbMapper;
use Auth\Core\Service\Auth as AuthService;
use Auth\Core\Adapter\Adapter as AuthAdapter;
use Auth\Core\Adapter\Listener as AuthListener;
use Faker\Factory as FakerFactory;
use Auth\Mapper\Users as UsersMapper;

class AuthTest extends AppTestCase
{
    public function setUp()
    {
        parent::setUp();
        AbstractDbMapper::configure('sqlite:./data/database/test_database.sqlite');
        AbstractDbMapper::configure('return_result_sets', true);
        AbstractDbMapper::get_db() -> beginTransaction();

        AuthService::getInstance() -> init($this -> app);
    } // setUp()


    public function tearDown()
    {
        AbstractDbMapper::get_db() -> rollBack();
        AuthService::getInstance() -> logout();
    } // tearDown()


    public function testGetApp()
    {
        $this -> assertInstanceOf('Core\App', AuthService::getInstance() -> app());
    } // testGetApp()


    public function testSuccessfulLogin()
    {
        $mapper = new UsersMapper($this -> app);
        $faker = FakerFactory::create();

        $table      = $mapper -> getTable();
        $login      = 'admin';
        $passwd     = 'password';
        $passwdHash = '$2y$14$phTLJz3QR2UkvgF6P0U2P.Gif3X3S5QBZPUgB9q4QayYPrboduZCe';
        $loginSuccessCode = 1;
        $adminId = 2;

        for ($i = 1; $i <= 3; $i++) {
            $user = $table -> create();
            $user -> login    = ($i === $adminId) ? $login : $faker -> userName;
            $user -> password = ($i === $adminId) ? $passwdHash : $faker -> md5;
            $user -> save();
        }
        $result = AuthService::getInstance() -> login($login, $passwd);

        $this -> assertEquals($result -> getCode(), $loginSuccessCode);
        $this -> assertEquals($result -> getIdentity(), $adminId);
    } // testSuccessfulLogin()


    public function testAuthenticateMethod()
    {
        $login      = 'admin';
        $passwd     = 'password';
        $passwdHash = '$2y$14$phTLJz3QR2UkvgF6P0U2P.Gif3X3S5QBZPUgB9q4QayYPrboduZCe';

        $user = (new UsersMapper($this -> app)) -> getTable() -> create();
        $user -> login    = $login;
        $user -> password = $passwdHash;
        $user -> save();

        $adapter = new AuthAdapter();
        $adapter -> setIdentity($login);
        $adapter -> setCredential($passwd);

        $adapter -> getEventManager() -> attachAggregate(new AuthListener());
        $result = AuthService::getInstance() -> authenticate($adapter);
        $this -> assertInstanceOf('App\Auth\Core\Result', $result);
    }


    public function testHasIdentitySuccess()
    {
        $authService = AuthService::getInstance();
        $login      = 'admin';
        $passwd     = 'password';
        $passwdHash = '$2y$14$phTLJz3QR2UkvgF6P0U2P.Gif3X3S5QBZPUgB9q4QayYPrboduZCe';

        $user = (new UsersMapper($this -> app)) -> getTable() -> create();
        $user -> login    = $login;
        $user -> password = $passwdHash;
        $user -> save();
        $result = $authService -> login($login, $passwd);

        $this -> assertTrue($authService -> hasIdentity());
    }


    public function testHasIdentityFail()
    {
        $authService = AuthService::getInstance();
        $login      = 'admin';
        $passwdHash = '$2y$14$phTLJz3QR2UkvgF6P0U2P.Gif3X3S5QBZPUgB9q4QayYPrboduZCe';

        $user = (new UsersMapper($this -> app)) -> getTable() -> create();
        $user -> login    = $login;
        $user -> password = $passwdHash;
        $user -> save();

        $this -> assertNull($authService -> getIdentity());

        $result = $authService -> login($login, 'hello');
        $this -> assertFalse($authService -> hasIdentity());

        $result = $authService -> login('fake_user', 'hello');
        $this -> assertFalse($authService -> hasIdentity());
    }


    public function testGetIdentity()
    {
        $authService = AuthService::getInstance();
        $login      = 'admin';
        $passwd     = 'password';
        $passwdHash = '$2y$14$phTLJz3QR2UkvgF6P0U2P.Gif3X3S5QBZPUgB9q4QayYPrboduZCe';

        $user = (new UsersMapper($this -> app)) -> getTable() -> create();
        $user -> login    = $login;
        $user -> password = $passwdHash;
        $user -> save();

        $result = $authService -> login($login, 'hello');
        $this -> assertNull($authService -> getIdentity());

        $result = $authService -> login($login, $passwd);
        $this -> assertEquals($authService -> getIdentity(), 1);
    }

}
