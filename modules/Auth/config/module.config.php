<?php

use Auth\AuthConfig;

return [
    'dependencies' => [
        'invokables' => [

        ],
        'factories' => [
            Auth\Action\UserBruteforceProtection::class => Auth\Action\UserBruteforceProtectionFactory::class,
            Auth\Action\Login::class => Auth\Action\LoginFactory::class,
            Auth\Action\Logout::class => Auth\Action\LogoutFactory::class,

            Auth\Options\ModuleOptions::class => Auth\Options\Factory::class,
            Auth\Form\Login::class => Auth\Form\LoginFactory::class,

            Auth\Model\Storage\UsersInterface::class => Auth\Model\Storage\Db\UsersFactory::class,
            Auth\Model\Repository\UsersInterface::class => Auth\Model\Repository\UsersFactory::class,

            Auth\Model\Storage\LockoutInterface::class => Auth\Model\Storage\Db\LockoutFactory::class,
            Auth\Model\Repository\LockoutInterface::class => Auth\Model\Repository\LockoutFactory::class,

            Auth\Service\User::class => Auth\Service\UserFactory::class,
            Auth\Service\UserIdentity::class => Auth\Service\UserIdentityFactory::class,
            Auth\Service\Adapter\AuthInterface::class => Auth\Service\Adapter\AuthFactory::class,
            Auth\Service\Auth::class => Auth\Service\AuthFactory::class,
            Zend\Authentication\AuthenticationServiceInterface::class => Auth\Service\ZendAuthFactory::class,
            Auth\Service\BruteforceProtection\User::class => Auth\Service\BruteforceProtection\UserFactory::class,

            Auth\Middleware\LoginRequired::class => Auth\Middleware\LoginRequiredFactory::class
        ],
    ],


    'routes' => [
        [
            'name' => 'login',
            'path' => '/login',
            'middleware' => [
                Auth\Action\UserBruteforceProtection::class,
                Auth\Action\Login::class,
            ],
            'allowed_methods' => ['GET', 'POST']
        ],
        [
            'name' => 'logout',
            'path' => '/logout',
            'middleware' => Auth\Action\Logout::class,
            'allowed_methods' => ['GET']
        ]
    ],


    'templates' => [
        'map' => [
            'layout::auth' => __DIR__ . '/../templates/layout/auth.phtml',
            'auth::login'  => __DIR__ . '/../templates/auth/login.phtml'
        ],
        'paths' => [
            'auth'    => [__DIR__ . '/../templates/auth'],
            'layout' => [__DIR__ . '/../templates/layout'],
        ],
    ],

    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../data/languages',
                'pattern' => '%s.mo',
                'text_domain' => AuthConfig::MODULE_NAME
            ]
        ]
    ]

];