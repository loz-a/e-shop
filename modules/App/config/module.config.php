<?php
use Zend\View\Helper\FlashMessenger;
use App\View\Helper\FlashMessages;

return [
    'dependencies' => [
        'invokables' => [
        ],
        'factories' => [
        ],
    ],

    'view_helpers' => [
        'invokables' => [
            'flashMessages' => FlashMessages::class,
        ],
        'factories' => [
            FlashMessenger::class => App\View\Helper\Service\FlashMessengerFactory::class,
        ]
    ]

];
