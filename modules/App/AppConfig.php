<?php
namespace App;

use Zend\Loader\AutoloaderFactory;

class AppConfig
{
    public function __invoke()
    {
        AutoloaderFactory::factory(array(
            //        'Zend\Loader\ClassMapAutoloader' => array(
            //            __DIR__ . '/autoload_classmap.php',
            //        ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        ));

        return require __DIR__ . '/config/module.config.php';
    } // __invoke()
}