<?php
namespace App\Middleware;

use Interop\Container\ContainerInterface;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class ZendFlashMiddlewareFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $flashMessenger = $container->get(FlashMessenger::class);

        return function($request, $response, $next) use ($flashMessenger) {
            return $next(
                $request->withAttribute('flash', $flashMessenger),
                $response
            );
        };
    } // __invoke()
}