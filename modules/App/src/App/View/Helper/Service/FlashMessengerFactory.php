<?php
namespace App\View\Helper\Service;

use Interop\Container\ContainerInterface;
use Zend\View\Helper\FlashMessenger;
use Zend\Mvc\Controller\Plugin\FlashMessenger as FlashMessengerPlugin;


class FlashMessengerFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $name
     * @param null|array $options
     * @return FlashMessenger
     */
    public function __invoke(ContainerInterface $container, $name, array $options = null)
    {
        // test if we are using Zend\ServiceManager v2 or v3
        if (! method_exists($container, 'configure')) {
            $container = $container->getServiceLocator();
        }
        $helper         = new FlashMessenger();
        $flashMessenger = $container->get(FlashMessengerPlugin::class);

        $helper->setPluginFlashMessenger($flashMessenger);

        return $helper;
    }
}