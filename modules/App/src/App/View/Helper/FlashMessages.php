<?php
namespace App\View\Helper;

use Zend\Mvc\Controller\Plugin\FlashMessenger as FlashMessengerPlugin;
use Zend\View\Helper\AbstractHelper;

class FlashMessages extends AbstractHelper
{
    public function __invoke()
    {
        /**
         * @var \Zend\View\Helper\FlashMessenger $flashMessengerHelper
         */
        $flashMessengerHelper = $this->getView()->flashMessenger();

        $htmlArr = [
            $flashMessengerHelper->render(FlashMessengerPlugin::NAMESPACE_DEFAULT),
            $flashMessengerHelper->renderCurrent(FlashMessengerPlugin::NAMESPACE_DEFAULT),
            $flashMessengerHelper->render(FlashMessengerPlugin::NAMESPACE_ERROR),
            $flashMessengerHelper->renderCurrent(FlashMessengerPlugin::NAMESPACE_ERROR),
            $flashMessengerHelper->render(FlashMessengerPlugin::NAMESPACE_INFO),
            $flashMessengerHelper->renderCurrent(FlashMessengerPlugin::NAMESPACE_INFO),
            $flashMessengerHelper->render(FlashMessengerPlugin::NAMESPACE_SUCCESS),
            $flashMessengerHelper->renderCurrent(FlashMessengerPlugin::NAMESPACE_WARNING),
        ];

        return implode('', array_filter($htmlArr, 'strlen'));
    } // __invoke()
}